* clone repo and move to project directory
* create virtual environment, activate it and install requirements
* run flask application to be able run selenium tests
```
#!

python app.py
python test.py
```

* run data transformation test
```
#!

python data_transformation.py < data_transformation.txt
```
