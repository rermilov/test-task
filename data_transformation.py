def main():

    input_data = []

    # Loop to collect data from input
    # Stops when no data provided
    while True:
        try:
            row = input()
        except EOFError:
            break
        else:
            input_data.append(
                [''.join(e for e in string if e.isalnum() or e == ' ').strip() for string in row.split(',')])

    caption = input_data.pop(0)
    caption, years = caption[:2] + ['year', 'median value'], caption[2:]

    print(','.join(caption))

    for data in input_data:
        for i in map(lambda x: data[:2] + list(x), zip(years, data[2:])):
            print(','.join(i))


if __name__ == '__main__':
    main()
