import tempfile
import unittest
from selenium import webdriver


class NoSuchProductException(Exception):
    pass


mark_up = (
    "<table><tbody>"
    "<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td></tr>"
    "<tr><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td></tr>"
    "<tr><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td></tr>"
    "</tbody></table>"
)

mark_up2 = (
    "<table><tbody>"
    "<tr><td>1</td><td>2</td><td>3</td></tr>"
    "<tr><td>4</td><td>5</td><td>6</td></tr>"
    "<tr><td>7</td><td>8</td><td>9</td></tr>"
    "<tr><td>10</td><td>11</td><td>12</td></tr>"
    "<tr><td>13</td><td>14</td><td>15</td></tr>"
    "</tbody></table>"
)

mark_up3 = (
    '<!DOCTYPE html>' +
    '<html>' +
    '<head>' +
    '<title></title>' +
    '</head>' +
    '<body>' +
    '<table>' +
    '<tr>' +
    '<th>Product name</th>' +
    '<th>Price</th>' +
    '<th>Action</th>' +
    '</tr>' +
    '<tr>' +
    '<td>iPhone 6</td>' +
    '<td>$849</td>' +
    '<td><input type="button" name="Add to Cart" value="Add to Cart" /></td>' +
    '</tr>' +
    '<tr>' +
    '<td>iPad Air</td>' +
    '<td>$499</td>' +
    '<td><input type="button" name="Add to Cart" value="Add to Cart" /></td>' +
    '</tr>' +
    '<tr>' +
    '<td>MacBook Air</td>' +
    '<td>$899</td>' +
    '<td><input type="button" name="Add to Cart" value="Add to Cart" /></td>' +
    '</tr>' +
    '</table>' +
    '</body>' +
    '</html>'
)


class SearchNumber(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Firefox()

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()

    @staticmethod
    def make_tmp_page(src):
        f, p = tempfile.mkstemp(".html")
        fw = open(f, "w")
        fw.write(src)
        fw.close()
        return 'file://{}'.format(p)

    def search_number(self, number):
        element = self.driver.find_element_by_xpath('//table//tr/td[text()={}]'.format(number))
        col = len(element.find_elements_by_xpath('./preceding-sibling::td')) + 1
        row = len(element.find_elements_by_xpath('./parent::tr/preceding-sibling::tr')) + 1
        return row, col

    def add_product_to_cart(self, product_name):
        for tr in self.driver.find_elements_by_tag_name('tr'):
            for td in tr.find_elements_by_tag_name('td'):
                if product_name == td.text:
                    tr.find_element_by_tag_name('input').click()
                    return
        raise NoSuchProductException('{} does not exist'.format(product_name))

    def test_search_eight(self):
        self.driver.get(self.make_tmp_page(mark_up))
        row, col = self.search_number(7)
        self.assertEqual((row, col), (2, 2))

    def test_search_another_eight(self):
        self.driver.get(self.make_tmp_page(mark_up2))
        row, col = self.search_number(7)
        self.assertEqual((row, col), (3, 1))

    def test_search_and_click_no_xpath(self):
        self.driver.get(self.make_tmp_page(mark_up3))
        self.assertEqual(self.add_product_to_cart('MacBook Air'), None)
        with self.assertRaises(NoSuchProductException):
            self.add_product_to_cart('No such element')


if __name__ == '__main__':
    unittest.main()
